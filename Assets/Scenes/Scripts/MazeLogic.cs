﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MazeLogic : MonoBehaviour
{
    public GameObject _parent;
    public GameObject _message;
    public GameObject _goalMessage;

    private bool animating = false;
    private bool touched = false;
    private int mapX = 0;
    private int mapY = 0;
    private int goalX = 1;
    private int goalY = 1;
    private string nowMapName = "map1";
    private string[] mapNames = new string[] { "map1", "map2", "map3"};

    // Start is called before the first frame update
    void Start()
    {
        updatePosition();
        initMaze(nowMapName);
    }

    // Update is called once per frame
    void Update()
    {
        if (!animating)
        {
            if (checkGoal())
            {
                if (Input.GetKeyDown(KeyCode.Return))
                {
                    nowMapName = nextMap();
                    initMaze(nowMapName);
                }
            }
            else
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    StartCoroutine(movePlayer(1));
                }
                else if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    StartCoroutine(movePlayer(-1));
                }
                else if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    StartCoroutine(turnPlayer(Vector3.down));
                }
                else if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    StartCoroutine(turnPlayer(Vector3.up));
                }
            }

        }
    }

    IEnumerator turnPlayer(Vector3 direction)
    {
        animating = true;
        var oldRotate = transform.localRotation;
        var newRotate = oldRotate * Quaternion.AngleAxis(90, direction);

        for (float t = 0; t < 0.25f; t += Time.deltaTime)
        {
            var rotation = Quaternion.Slerp(oldRotate, newRotate, t * 4);
            transform.localRotation = rotation;
            yield return null;
        }
        transform.localRotation = newRotate;
        animating = false;
    }

    IEnumerator movePlayer(int direction)
    {
        animating = true;
        var oldPos = transform.position;
        var newPos = oldPos + transform.forward * 4 * direction;
        for (float t = 0; t < 0.25f; t += Time.deltaTime)
        {
            var pos = Vector3.Lerp(oldPos, newPos, t * 4);
            transform.position = pos;
            if (touched)
            {
                transform.position = oldPos;
                animating = false;
                touched = false;
                yield break;
            }
            yield return null;
        }
        transform.position = newPos;
        animating = false;
        updatePosition();
    }

    private void OnTriggerEnter(Collider other)
    {
        touched = true;
    }

    void initMaze(string mapName)
    {
        var wallPrefab = Resources.Load<GameObject>("Wall");

        var mapText = loadMap(mapName);
        var mapData = parseMap(mapText);

        clearMap();
        for(int i = 0; i < mapData["xMax"]; i++)
        {
            for(int j = 0; j < mapData["yMax"]; j++)
            {
                if (safeGet(mapData, string.Format("{0}.{1}.nWall", i, j)) == 1)
                {
                    Instantiate(wallPrefab, new Vector3(i * -4, 2, j * 4 - 2), Quaternion.identity, _parent.transform);
                }
                if (safeGet(mapData, string.Format("{0}.{1}.wWall", i, j)) == 1)
                {
                    Instantiate(wallPrefab, new Vector3((i * -4) + 2, 2, j * 4), Quaternion.Euler(0, 90.0f, 0), _parent.transform);
                }
                if (safeGet(mapData, string.Format("{0}.{1}.floor", i, j)) == 1)
                {
                    mapX = i;
                    mapY = j;
                }
                if (safeGet(mapData, string.Format("{0}.{1}.floor", i, j)) == 2)
                {
                    goalX = i;
                    goalY = j;
                }
            }
        }
        warp(mapX, mapY);
        _goalMessage.SetActive(false);
    }

    int safeGet(Dictionary<string, int> dict, string key)
    {
        try
        {
            int result = dict[key];
            return result;
        }catch(KeyNotFoundException e)
        {

        }
        return -1;
    }

    void updatePosition()
    {
        mapX = (int)((transform.position.x * -1 + 0.5f) / 4);
        mapY = (int)((transform.position.z + 0.5f) / 4);
        updatePositionMessage();
        updateGoalMessage();
    }

    void updatePositionMessage()
    {
        _message.GetComponent<TextMeshProUGUI>().text = string.Format("GOAL...\nx = {0}\ny = {1}", goalX - mapX, goalY - mapY);
    }

    void updateGoalMessage()
    {
        _goalMessage.SetActive(checkGoal());
    }

    void warp(int x, int y)
    {
        transform.position = new Vector3(x * -4, transform.position.y, y * 4);
        updatePosition();
    }

    bool checkGoal()
    {
        return ((mapX == goalX) && (mapY == goalY));
    }

    void clearMap()
    {
        foreach(Transform childTrans in _parent.transform)
        {
            GameObject.Destroy(childTrans.gameObject);
        }
    }

    string nextMap()
    {
        for(int i = 0; i < mapNames.Length; i++)
        {
            if(mapNames[i] == nowMapName)
            {
                return mapNames[(i + 1) % mapNames.Length];
            }
        }
        return "map1";
    }

    string loadMap(string mapName)
    {
        var ta = Resources.Load<TextAsset>(mapName);
        return ta.text;
    }

    Dictionary<string, int> parseMap(string mapText)
    {
        var result = new Dictionary<string, int>();
        result["xMax"] = 0;
        result["yMax"] = 0;

        var lines = mapText.Split('\n');
        for(var i = 0; i < lines.Length; i++)
        {
            var tokens = lines[i].ToCharArray();
            for(var j = 0; j < tokens.Length; j++)
            {
                if (tokens[j] == '\n')
                {

                }
                else if(tokens[j] == '-')
                {
                    result[string.Format("{0}.{1}.nWall", j / 2, i / 2)] = 1;
                }
                else if (tokens[j] == '|')
                {
                    result[string.Format("{0}.{1}.wWall", j / 2, i / 2)] = 1;
                }
                else if (tokens[j] == 'S' || tokens[j] == 's')
                {
                    result[string.Format("{0}.{1}.floor", j / 2, i / 2)] = 1;
                }
                else if (tokens[j] == 'G' || tokens[j] == 'g')
                {
                    result[string.Format("{0}.{1}.floor", j / 2, i / 2)] = 2;
                }

                result["xMax"] = Mathf.Max(result["xMax"], j / 2 + 1);
            }
            result["yMax"] = Mathf.Max(result["yMax"], i / 2 + 1);
        }

        return result;
    }
}
